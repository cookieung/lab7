import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * It's an application that converts the require unit.
 * @author Salilthip 5710546640
 *
 */
public class ConverterUI extends JFrame {

	private UnitConverter unitconverter;
	private String s;
	private Unit unit1, unit2;
	private Container contents;
	private LayoutManager layout;
	private JButton convertButton, clearButton;
	private JTextField inputValue, resultField;
	private JComboBox<Unit> before;
	private JComboBox<Unit> after;

	/**
	 * Construct a frame for run application.
	 * @param uc is a class for convert the unit.
	 */
	public ConverterUI(UnitConverter uc) {
		this.unitconverter = uc;
		this.setTitle("Simple Converter");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponents();
		this.pack();
	}

	/**
	 * Initialize components in the window.
	 */
	private void initComponents() {
		before = new JComboBox<>(unitconverter.getUnits());
		after = new JComboBox<>(unitconverter.getUnits());
		contents = new Container();
		layout = new FlowLayout();
		contents.setLayout(layout);
		resultField = new JTextField(10);
		inputValue = new JTextField(10);
		resultField.setEditable(false);
		convertButton = new JButton("Convert");
		clearButton = new JButton("Clear");
		contents.add(inputValue);
		contents.add(before);
		contents.add(resultField);
		contents.add(after);
		contents.add(convertButton);
		contents.add(clearButton);

		this.add(contents);
		ActionListener listener = new ConvertButtonListener();
		convertButton.addActionListener(listener);
		ActionListener listener2 = new ClearButtonListener();
		clearButton.addActionListener(listener2);
		inputValue.addActionListener(listener);
	}

	/**
	 * It's a controller of this application on the convertButton.
	 *
	 */
	public class ConvertButtonListener implements ActionListener {
		/**
		 *  method to perform action when the button is pressed 
		 */
		public void actionPerformed(ActionEvent evt) {
			s = inputValue.getText();
			try {
				if (s.length() > 0) {
					unit1 = (Unit) before.getSelectedItem();
					unit2 = (Unit) after.getSelectedItem();
					double value = Double.valueOf(s);
					double result = unitconverter.convert(value, unit1, unit2);
					resultField.setText(String.format("%.8f", result));
				}
			} catch (NumberFormatException e) {
				JOptionPane.showMessageDialog(inputValue, "You must input number on this field");
			}

		}
	}

	/**
	 * It's a controller of this application on the clearButton.
	 *
	 */
	public class ClearButtonListener implements ActionListener {
		/** method to perform action when the button is pressed */
		public void actionPerformed(ActionEvent evt) {
			inputValue.setText("");
			resultField.setText("");
		}
	}

	/**
	 * Show this application.
	 */
	public void run() {
		this.setVisible(true);
	}

	/**
	 * It's main of Application.
	 * @param args
	 */
	public static void main(String[] args) {
		ConverterUI a = new ConverterUI(new UnitConverter());
		a.run();
	}

}
