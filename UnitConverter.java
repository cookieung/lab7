
public class UnitConverter {

	public static void main(String[]args){
		Length kilometer = Length.KILOMETER;
		System.out.print(kilometer.convertTo(3, Length.YARD));
	}
	
	public double convert(double amount, Unit fromUnit,Unit toUnit){
		return fromUnit.convertTo(amount, toUnit);
	}
	
	public Unit[] getUnits(){
		return Length.values();
	}
	
}
